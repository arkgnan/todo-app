- install jwt dengan composer  
`composer require tymon/jwt-auth`  
- copy file env.example menjadi env, setting & create database  
- jalankan `php artisan migrate`  
- setup jwt dengan laravel [panduan setup](https://jwt-auth.readthedocs.io/en/develop/laravel-installation/)  